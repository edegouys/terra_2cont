resource "azurerm_resource_group" "rg" {
    name= "${var.name_rg}"
    location = "${var.location}"

    tags {
        owner= "${var.owner}"
    }
}
resource "azurerm_virtual_network" "vNet" {
    name= "${var.name_vnet}"
    address_space= "${var.address_space}"
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  
}
resource "azurerm_subnet" "subNet" {
    name= "${var.name_subNet}"
    resource_group_name= "${azurerm_resource_group.rg.name}" 
    virtual_network_name= "${azurerm_virtual_network.vNet.name}"
    address_prefix= "${var.address_prefix}"
}

# resource "azurerm_container_registry" "CR1" {
#   name= "${var.name_CR}"
#   resource_group_name= "${azurerm_resource_group.rg.name}" 
#   location= "${azurerm_resource_group.rg.location}"
#   sku= "standard"
#   admin_enabled= false
# }

resource "azurerm_container_group" "CG1" {
  name= "${var.name_CG}"
  resource_group_name= "${azurerm_resource_group.rg.name}" 
  location= "westeurope"
  ip_address_type= "${var.IP_type}"
  dns_name_label= "${var.name_DNS}"
  os_type= "${var.OS_type}"

  container {
    name   = "nginx"
    image  = "nginx"
    cpu    = "1"
    memory = "1.5"

    ports {
      port= "80"
      protocol= "TCP"
    }
  }

    container {
    name   = "hello-world"
    image  = "hello-world"
    cpu    = "1"
    memory = "1.5"

    ports {
      port= "8080"
      protocol= "TCP"
    }
  }
}